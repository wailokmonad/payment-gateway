FROM node:12

WORKDIR /

run mkdir /views
run mkdir /model

COPY package.json ./
COPY index.js ./
COPY config.js ./

COPY views/ views/
COPY model/ model/

RUN npm install

EXPOSE 3000

CMD [ "node", "index.js" ]