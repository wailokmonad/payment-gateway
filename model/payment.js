let mongoose = require('mongoose');

var paymentSchema = new mongoose.Schema({
  name: String,
  phone: String,
  currency: String,
  ref: String,
  price: Number
});

module.exports = mongoose.model('payment', paymentSchema);