# payment-gateway

## Requirement
This application is container-based. Please make sure ***docker*** and ***docker-compose*** are both installed on your machine.

## Setup
This application should accept payment using the ***stripe*** payment gateway.

First and foremost, please replace ***STRIPE_SECRET_KEY*** and ***PUBLISH_KEY*** in the ***config.js*** with your own stripe production key.

Secondly, set up the webhooks endpoint in your stripe dashboard (sidebar menu -> develops -> webhooks). It should be ***http://your-domain.com:3000/webhooks***. After that, replace ***STRIPE_WEBHOOK_SECRET*** in the ***config.js*** with the webhook secret. Using webhook to confirm payment is recommended offically by stripe.


Finally, in the repository directory, run

```bash 
docker-compose up -d
```

It should spin up three containers. Now let's run

```bash 
docker ps
```
You should see something similar

```bash 
CONTAINER ID        IMAGE                  COMMAND                  CREATED             STATUS              PORTS                    NAMES
492cbd53c8c7        container_backend      "docker-entrypoint.s…"   About an hour ago   Up About an hour    0.0.0.0:3000->3000/tcp   nodejs
e34766a392fb        mongo                  "docker-entrypoint.s…"   About an hour ago   Up About an hour    27017/tcp                mongo
45a272670400        bitnami/redis:latest   "/opt/bitnami/script…"   About an hour ago   Up About an hour    6379/tcp                 redis
```

## Architecture

As you can see, there are 3 containers running in your machine.
1) A Nodejs Backend which listens on ***3000*** port. It provides the frontend and backend functionality. It is connected to Mongodb and Redis.

2) Mongodb. Direct communication with it outside the containers environment is ***NOT*** supported. It serves to save the payment record.

3) Redis. Direct communication with it outside the containers environment is ***NOT*** supported. It serves as a cache for speeding up the query.

All the data from mongodb is bound on the ***data*** folder. 

## Database Schema
Please check out the ***payment.js*** in the ***model*** folder.

## Usage
In your browser, just head over to ***http://your-domain.com:3000*** to make payment, and ***http://your-domain.com:3000/check*** to check payment.



























