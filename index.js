let mongoose = require('mongoose');
let express = require('express')
let app = express()
let bodyParser = require('body-parser')
let cors = require('cors')
let redis = require("async-redis");
let ejs = require('ejs')
let client = redis.createClient({host:"redis"});
let Payment = require('./model/payment')
let config = require('./config')
const STRIPE_WEBHOOK_SECRET = config.STRIPE_WEBHOOK_SECRET
const STRIPE_SECRET_KEY = config.STRIPE_SECRET_KEY
const PUBLISH_KEY = config.PUBLISH_KEY
const EXPIRE = config.EXPIRE
let stripe = require("stripe")(STRIPE_SECRET_KEY);

let mongoDB = 'mongodb://mongo/';
mongoose.connect(mongoDB, { 
  useNewUrlParser: true,
  useUnifiedTopology: true 
});
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));



app.use(
  express.json({
    verify: function(req, res, buf) {
      if (req.originalUrl.startsWith("/webhooks")) {
        req.rawBody = buf.toString();
      }
    }
  })
);


app.use(cors())
app.use( bodyParser.urlencoded({ extended: false })  )
app.use( bodyParser.json() )
app.set('view engine', 'ejs');
//app.use(express.static( __dirname + "/views"));

app.get('/', async function (req, res) {
  res.render('create', {PUBLISH_KEY});
})

app.get('/check', async function (req, res) {
  res.render('check');
})

app.get('/payment', async function (req, res) {

  let key = typeof req.query.key == 'string' ? req.query.key.trim() : "";
  let value = typeof req.query.value == 'string' ? req.query.value.trim() : "";
  let data;

  if( !key ){
    res.send({success:false, message:"key is required"})
    return
  }

  if( !value ){
    res.send({success:false, message:"value is required"})
    return
  }

  try{

    data = await client.get(`${key}_${value}`);

    if(data){
      console.log("===== from redis ======")
      res.send({success:true, data: JSON.parse(data) })
      return
    }

    let query = Payment.find();

    if( key == "name"){
      query.where("name", value)
    }else if( key == "ref" ){
      query.where("ref", value)
    }else{
      res.send({success:false, message:"Invalid Key"})
      return
    }
    
    data = await query.exec()

    if(data.length > 0){
      await client.set(`${key}_${value}`, JSON.stringify(data));
      await client.expire(`${key}_${value}`, EXPIRE)
      res.send({success:true, data})
      return
    }

    res.send({success:false, message:"Record Not Found"})

  }catch(e){
    res.send({success:false, message:e.message})
    return
  }

})

app.post('/payment', async function (req, res) {

  let name = typeof req.body.name == 'string' ? req.body.name.trim() : "";
  let phone = typeof req.body.phone == 'string' ? req.body.phone.trim() : "";
  let currency = typeof req.body.currency == 'string' ? req.body.currency.trim() : "";
  let price = typeof req.body.price == 'string' ? req.body.price.trim() : "";
  price = parseInt(price)
  phone = phone.replace(/\s/g, '');

  if(!name){
    res.send({success:false, message:"name is required"})
    return
  }

  if(!phone){
    res.send({success:false, message:"phone is required"})
    return
  }

  if(phone.length != 8){
    res.send({success:false, message:"phone length must be 8"})
    return
  }

  if(!currency){
    res.send({success:false, message:"currency is required"})
    return
  }

  if( currency != "hkd" ){
    res.send({success:false, message:"Only HKD is supported. Other options are coming soon"})
    return
  }

  if( isNaN(price) || price <= 0 ){
    res.send({success:false, message:"price must be greater than zero"})
    return
  }


  try{

    let paymentIntent = await stripe.paymentIntents.create({
      amount: price * 100,
      currency: currency,
      payment_method_types: ['card'],
      metadata: {name,phone},
    });

    res.send({ success:true, data: { 
        clientSecret: paymentIntent.client_secret,
        Id: paymentIntent.id
      } 
    });

  }catch(e){

    res.send({ success:false, message:e.message })
  }
  
})

app.post("/webhooks", async (req, res) => {

  let data, eventType;
  let event;
  let signature = req.headers["stripe-signature"];

  try {
    event = stripe.webhooks.constructEvent(
      req.rawBody,
      signature,
      STRIPE_WEBHOOK_SECRET
    );
  } catch (err) {
      return res.sendStatus(400);
  }

  data = event.data;
  eventType = event.type;

  if (eventType === "payment_intent.succeeded") {

      try{

        console.log(`id: ${data.object.id}, name: ${data.object.metadata.name}`)

        let instance = new Payment({
          ref: data.object.id,
          name: data.object.metadata.name, 
          phone: data.object.metadata.phone, 
          currency: data.object.currency, 
          price: data.object.amount / 100
        });

        await instance.save()
  
      }catch(e){
        return res.sendStatus(500);
        
      }

  }

  return res.sendStatus(200);
  
});

app.listen(3000)



